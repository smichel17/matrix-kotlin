#####################################################
#                  Housekeeping                     #
#####################################################

COMMANDS:= rref invert math determinant diagonalize unit weight distance dot
UTILS:= entry.jar matrix.jar
KVER:= 1.2.41

kc:= tools/kotlinc/bin/kotlinc
kt:= tools/kotlinc/bin/kotlin

empty:=
space:= $(empty) $(empty)

main = $(shell X=$(1:%.jar=%Kt); echo $${X^})
classpath = $(if $1,-classpath $(subst $(space),:,$1),)

#####################################################
#                     Rules                         #
#####################################################

.DEFAULT_GOAL := help

.PHONY: help
help:
	@echo 'Available commands (default: $(.DEFAULT_GOAL)):'
	@echo '  setup:    Download dependencies (kotlin compiler) into the `tools` folder'
	@echo '  clean:    Delete jar files'
	@echo '  clobber:  Also delete `tools` folder'
	@echo '  help:     Print this message'
	@echo ''
# @echo 'Edit the Makefile to change these parameters.'

.PHONY: $(COMMANDS)
$(COMMANDS): %: %.jar $(UTILS) | $(kc)
	@$(kt) $(call classpath,$^) $(call main,$<)

.PHONY: *.kts
*.kts: $(UTILS) | $(kc)
	$(kc) $(call classpath,$^) -script $@

# Must manually specify dependencies for utils
matrix.jar: entry.jar
$(UTILS): %.jar: %.kt | $(kc)
	$(kc) $< $(call classpath,$(filter-out $<,$^)) -d $@

%.jar: %.kt $(UTILS) | $(kc)
	$(kc) $< -include-runtime $(call classpath,$(UTILS)) -d $@

.PHONY: setup
setup: $(kc)

$(kc):
	@rm -f kotlin-compiler-$(KVER).zip
	@rm -rf tools/kotlinc/
	mkdir -p tools/
	wget https://github.com/JetBrains/kotlin/releases/download/v$(KVER)/kotlin-compiler-$(KVER).zip
	unzip kotlin-compiler-$(KVER).zip
	mv kotlinc/ tools/kotlinc
	rm -f kotlin-compiler-$(KVER).zip

.PHONY: clean
clean:
	rm -f *.jar

.PHONY: clobber
clobber: clean
	rm -rf tools/

