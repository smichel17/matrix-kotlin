import matrix.*

fun main(args: Array<String>) {
    val A = readMatrix()
    val det = A.determinant()
    println("Determinant: $det")
}
