import matrix.*

fun main(args: Array<String>) {
    var A: Matrix? = null
    do {
        if (A != null) {
            println("Must be square matrix, please try again.")
        }
        A = readMatrix()
    } while (A != null && A.numRows != A.numCols)

    val I = A!!.identity()!!
    var lambda: Double
    while (true) {
        println("Please input an eigenvalue:")
        lambda = readLine()?.toDoubleOrNull() ?: break
        val X = A - I * lambda
        val R = X.rref()
        R.print()
    }
}
