import matrix.*
import entry.*
import kotlin.math.sqrt

fun main(args: Array<String>) {
    var A: Matrix? = null
    var B: Matrix? = null
    do {
        if (A != null && B != null) {
            println("Must be single row matrix, please try again.")
        }
        A = readMatrix()
        B = readMatrix()
    } while (A != null && A.numRows != 1 && B != null && B.numRows != 1)
    val a = A!![0]
    val b = B!![0]
    val ab = a.zip(b) { ai, bi -> ai - bi }
    val dot = ab.dot(ab)
    val len = if (dot.isConstant()) "${sqrt(dot.base)}" else "???"
    println("Distance is sqrt($dot) = $len")
}
