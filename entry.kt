package entry

data class Entry(
    val base: Double = 0.0,
    val variables: Map<String, Double> = emptyMap<String, Double>()
) {
    override fun toString(): String {
        return variables.toList().fold(pretty(base)) { acc, (v, c)->
            when {
                c == 0.0 ->
                    acc
                v.isNotEmpty() && c == 1.0 ->
                    acc + "+" + v
                v.isNotEmpty() && c == -1.0 ->
                    acc + "-" + v
                c > 0.0 ->
                    acc + "+" + pretty(c) + v
                else ->
                    acc + pretty(c) + v
            }
        }.removePrefix("0+").let {
            if (it.startsWith("0-")) it.drop(1) else it
        }
    }
}

fun pretty(x: Double): String {
    return x.toString().removeSuffix(".0").takeIf { it != "-0" } ?: "0"
}

fun Entry.isConstant(): Boolean = this.variables.isEmpty()

operator fun Entry.plus(constant: Double): Entry = this.copy(base = this.base + constant)
operator fun Entry.minus(constant: Double): Entry = this.copy(base = this.base - constant)

operator fun Entry.times(scalar: Double): Entry {
    val b = this.base * scalar
    val v = this.variables.mapValues { (_, v) -> v * scalar }
    return Entry(b, v)
}
operator fun Entry.div(scalar: Double): Entry {
    val b = this.base / scalar
    val v = this.variables.mapValues { (_, v) -> v / scalar }
    return Entry(b, v)
}

/* override operator fun Entry.equals(constant: Double): Boolean { */
/*     return this.variables.isEmpty() && this.base == constant */
/* } */

operator fun Entry.plus(other: Entry): Entry = this.applyOp(other, Double::plus)
operator fun Entry.minus(other: Entry): Entry = this.applyOp(other, Double::minus)

fun Entry.applyOp(other: Entry, op: Double.(Double) -> Double): Entry {
    val b = this.base.op(other.base)
    val v = this.variables.toMutableMap()
    other.variables.forEach { key, value ->
        v[key] = v.getOrDefault(key, 0.0).op(value)
    }
    return Entry(b, v)
}

operator fun Entry.times(new: Pair<String, Double>): Entry {
    val (newV, newC) = new
    val vs = this.variables.mapKeys { (v, _) -> v + newV }
                           .mapValues { (_, c) -> c * newC }
                           .plus(Pair(newV, this.base * newC))
    return Entry(0.0, vs)
}

operator fun Entry.times(other: Entry): Entry {
    return when {
        this.isConstant() -> other * this.base
        other.isConstant() -> this * other.base
        else -> {
            val first = this * other.base
            val rest = other.variables.map { (v, c) -> this * Pair(v, c) }
            rest.fold(first) { acc, e -> acc + e }
        }
    }
}

operator fun Entry.div(other: Entry): Entry {
    return if (this == Entry()) {
        Entry()
    } else if (this == other) {
        Entry(1.0)
    } else if (other.isConstant()) {
        this / other.base
    } else {
        val v = "($this/$other)"
        val e = Entry(0.0, mapOf(v to 1.0))
        println("Divided $this by $other, got $e")
        e
    }
}

fun String.toEntry(): Entry {
    if (this.isEmpty()) return Entry()

    val commaSeparated = this.first() + this.drop(1).replace("-",",-").replace("+",",")
    val parts = commaSeparated.split(",")

    var b = 0.0
    val v = mutableMapOf<String, Double>()
    parts.forEach { part ->
        val i = part.indexOfFirst { it in 'a'..'z' || it in 'A'..'Z' }
        when (i) {
            -1 -> b += part.toDouble()
            0 -> v[part] = v.getOrDefault(part, 0.0) + 1.0
            1 -> {
                val c = if (part.first() == '-') -1.0 else part.substring(0,1).toDouble()
                val l = part.substring(i, part.length)
                v[l] = v.getOrDefault(l, 0.0) + c
            }
            else -> {
                val c = part.substring(0,i).toDouble()
                val l = part.substring(i, part.length)
                v[l] = v.getOrDefault(l, 0.0) + c
            }
        }
    }
    return Entry(b, v)
}
