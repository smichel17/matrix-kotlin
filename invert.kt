import matrix.*

fun main(args: Array<String>) {
    val A = readMatrix()
    val I = A.inverted()
    if (I == null) {
        println("Matrix is not invertible")
    } else {
        println("Solution:")
        I.print()
    }
}
