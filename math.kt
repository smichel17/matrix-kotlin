import matrix.*

fun main(args: Array<String>) {
    var result: Matrix?
    do {
        val A = readMatrix()
        println("Please input one of: +, -, *")
        val op = readLine()
        val B = readMatrix()
        result = when(op) {
            "+" -> if (A.numRows != B.numRows || A.numCols != B.numCols) null else A + B
            "-" -> if (A.numRows != B.numRows || A.numCols != B.numCols) null else A - B
            "*" -> if (A.numCols != B.numRows) null else A * B
            else -> {
                println("Bad op")
                null
            }
        }
        result ?: println("Invalid dimensions or operator, try again")
    } while (result == null)

    result.print()
}
