package matrix

import entry.*

fun Vector.dot(other: Vector): Entry {
    if (size != other.size) {
        println("Vectors must be same length: (this, that)=($size, ${other.size})")
        return Entry()
    }
    val multiplied = other.mapIndexed { i, entry -> entry * this[i] }
    val dotProduct = multiplied.reduce { a, b -> a + b }
    return dotProduct
}

fun Matrix.determinant(): Entry {
    if (numRows != numCols) {
        println("ERROR: Can't compute determinant of non-square matrix")
        return Entry()
    }
    println("Finding determinant of:")
    this.print()
    return when (numRows) {
        0 -> {
            println("ERROR: no entries in matrix")
            Entry()
        }
        1 -> this[0][0]
        2 -> this[0][0] * this[1][1] - this[1][0] * this[0][1]
        else -> {
            println("Cofactor expansion along which row? Blank for diagonal.")
            val line = readLine()?.toIntOrNull()?.let { it - 1 }
            if (line == null) {
                println("Diagonal!")
                this.mapIndexed { i, r -> r[i] }.reduce { prod, entry -> prod * entry }
            } else {
                this[line].foldIndexed(Entry()) { col, sum, entry ->
                    val A = this.toMutableList<List<Entry>>().apply { removeAt(line) }.map {
                        it.toMutableList().apply { removeAt(col) }
                    }
                    val sign = Entry(-1.0).pow(line + col)
                    println("line, sign: $line, $sign")
                    sum + sign * entry * A.determinant()
                }
            }
        }
    }
}

private fun Entry.pow(n: Int): Entry {
    return when {
        n < 0 -> Entry(1.0) / this.pow(-n)
        n == 0 -> Entry(1.0)
        n == 1 -> this
        else -> this * this.pow(n-1)
    }
}

fun Matrix.inverted(): Matrix? {
    val I = this.identity() ?: return null
    val A = this.mapIndexed { i, row -> row + I[i] }
    val R = A.rref()
    val inverted = R.map { it.drop(this.size) }
    return inverted.takeIf { this.rref() == I }
}

fun Matrix.identity(): Matrix? {
    return if (numRows == numCols) identityMatrix(size) else null
}

fun identityMatrix(n: Int): Matrix {
    return Array(n) { row ->
        Array(n) { col ->
            if (row == col) Entry(1.0) else Entry(0.0)
        }.toList()
    }.toList()
}

fun Matrix.rref(work: Boolean = false): Matrix {
    val A = this.toMutableList()

    for (pRow in A.indices) {
        // Find the the first non-zero column
        val nonZeroRows = A.columns(pRow).map { col ->
            col.indexOfFirst { it != Entry() }
        }
        val pivotColumn = nonZeroRows.indexOfFirst { it != -1 }
        if (pivotColumn == -1) { break } // Everything is 0, we're done

        // Move the first non-zero row in that column to pivot position
        val nonZeroRow  = pRow + nonZeroRows[pivotColumn]
        A.swap(pRow, nonZeroRow)
        if (work && pRow != nonZeroRow) {
            println("Swapped rows $pRow and $nonZeroRow")
            A.print()
        }

        // Scale pivot row so pivot is 1
        val pivot = A[pRow][pivotColumn]
        if (pivot.isConstant() && pivot.base != 1.0) {
            A[pRow] = A[pRow].map { it / pivot }
            if (work) {
                println("Scaled pivot row by 1/$pivot")
                A.print()
            }
        }

        // Replacement to make numbers below and above pivot 0
        val otherRows = A.indices.toList().minus(pRow) // Don't erase the pivot row
        otherRows.forEach { row ->
            val ratio = A[row][pivotColumn]
            if (work) {
                println("R${row+1}=R${row+1}-$ratio*R${pRow+1}")
            }
            A[row] = A[row].mapIndexed { col, value ->
                value - A[pRow][col] * ratio
            }
        }
        if (work) {
            A.print()
        }
    }

    return A
}

typealias Matrix = List<List<Entry>>
typealias Vector = List<Entry>

val Matrix.numCols: Int get() = this.firstOrNull()?.size ?: 0
val Matrix.numRows: Int get() = this.size

fun Matrix.isValid(): Boolean {
    return this.isNotEmpty() && this.all { it.size == this[0].size }
}

fun <T>List<List<T>>.columns(from: Int = 0, to: Int = this.size): List<List<T>> {
    val sublist = this.subList(from, to)
    return this[0].indices.map {
        sublist.column(it)
    }
}

fun <T>List<List<T>>.column(index: Int): List<T> {
    return if (index < 0 || index >= this[0].size) {
        emptyList<T>()
    } else {
        this.map { it[index] }
    }
}

operator fun Matrix.plus(other: Matrix): Matrix {
    return this.zip(other) { row, otherRow ->
        row.zip(otherRow) { a, b -> a + b }
    }
}

operator fun Matrix.minus(other: Matrix): Matrix {
    return this.zip(other) { row, otherRow ->
        row.zip(otherRow) { a, b -> a - b }
    }
}

operator fun Matrix.times(other: Matrix): Matrix {
    return this.map { row ->
        other.columns().map { col ->
            row.zip(col) { a, b -> a * b }.reduce { acc, e -> acc + e }
        }
    }
}

operator fun Matrix.times(constant: Double): Matrix {
    return this.map { row -> row.map { entry -> entry * constant } }
}

/* fun <T>List<T>.swap(a: Int, b: Int): List<T> = this.toMutableList().apply { swap(a, b) } */

fun <T>MutableList<T>.swap(a: Int, b: Int) {
    if (a != b) {
        val temp = this[a]
        this[a] = this[b]
        this[b] = temp
    }
}

fun Matrix.print() {
    val strings = this.map { row ->
        row.map { "$it" }
    }
    val lengths = strings.columns().map { col ->
        col.map { it.length }.max() ?: 0
    }
    strings.forEach { row ->
        val padded = row.mapIndexed { i, v -> v.padStart(lengths[i]) }
        println("$padded")
    }
}

tailrec fun readMatrix(
        prompt: String = "Please input a space separated matrix:"
): Matrix {
    println(prompt)

    val A = mutableListOf<List<Entry>>()
    do {
        val line: String = readLine() ?: ""
        if (line.isNotEmpty()) {
            val delimiter = if (line.contains(",")) { "," } else { " " }
            val row = line.split(delimiter).map { it.toEntry() }
            A.add(row)
            if (!A.isValid()) {
                A.removeAt(A.lastIndex)
                println("Invalid row (wrong length), please try again")
            }
        }
    } while (line.isNotEmpty())

    return A.takeIf { it.isValid() } ?:
        readMatrix("Invalid matrix (inconsistent row length); please try again:")
}
