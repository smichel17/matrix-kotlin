import matrix.*

fun main(args: Array<String>) {
    val input = readMatrix()
    val output = input.rref(true)
    println("Solution:")
    output.print()
}
