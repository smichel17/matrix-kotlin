import matrix.*
import entry.*
import kotlin.math.sqrt

fun main(args: Array<String>) {
    var A: Matrix? = null
    do {
        if (A != null) {
            println("Must be single row matrix, please try again.")
        }
        A = readMatrix()
    } while (A != null && A.numRows != 1)
    val row = A!![0]
    val sum = row.map { it * it }.reduce { a, b -> a + b }
    val len = if (sum.isConstant()) "${sqrt(sum.base)}" else "???"
    println("Length is sqrt($sum) = $len")
    if (sum.isConstant()) {
        val unit = row.map { it / sqrt(sum.base) }
        println("Unit vector in this direction: $unit")
    }
}
