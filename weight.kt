import matrix.*
import entry.*

fun main(args: Array<String>) {
    var A: Matrix? = null
    var B: Matrix? = null
    do {
        if (A != null && B != null) {
            println("Must be single row matrix, please try again.")
        }
        A = readMatrix()
        B = readMatrix()
    } while (A != null && A.numRows != 1 && B != null && B.numRows != 1)
    val a = A!![0]
    val b = B!![0]
    val numerator = a.dot(b)
    val denominator = b.dot(b)
    val scalar = numerator / denominator
    println("Scalar is $numerator/$denominator = $scalar")
    val scaled = b.map { it * scalar }
    println("Scaled vector is: $scaled")
}
